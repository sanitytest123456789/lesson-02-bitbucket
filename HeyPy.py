command = ""

started = False



start_service = "Service started..."

start_service_already = "Service already started"



while True:

    command = input("> ").lower()

    if command == "start":

        if started:

            print(f"{start_service_already}")

        else:

            started = True

            print(f"{start_service}")

    elif command == "stop":

        if not started:

            print("Service already stopped")

        else:

            started = False

            print("Service stopped")

    elif command == "help":

        print("""

start -- Start service

stop -- Stop service

quit -- Exit service

    """)

    elif command == "quit":

        print("bye")

        break

    else:

        print("I don't understand")


